import { Ilop } from "./../models/lop.interface";
export const dataLOP: Ilop[] = [
    {
        idLop: "1",
        nameLop: "toán"
    },
    {
        idLop: "2",
        nameLop: "văn"
    },
    {
        idLop: "3",
        nameLop: "anh"
    }
]