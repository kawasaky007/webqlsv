import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailClassComponent } from './detail-class/detail-class.component';
import { DSSVComponent } from './dssv/dssv.component';
import { DetailStudentComponent } from './detail-student/detail-student.component';


const routes: Routes = [
{
  path:'detailclass',component:DetailClassComponent
},
{
  path:'detailDSSV/:id',component:DSSVComponent
},
{
  path:'detailStudent/:id',component:DetailStudentComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
