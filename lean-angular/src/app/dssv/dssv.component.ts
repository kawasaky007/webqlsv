import { Component, OnInit } from '@angular/core';
import { StudentService } from '../student.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-dssv',
  templateUrl: './dssv.component.html',
  styleUrls: ['./dssv.component.css']
})
export class DSSVComponent implements OnInit {
  mangsinhvien = [];
  constructor(private studentService:StudentService, private activedRoute:ActivatedRoute,private router:Router) { }

  ngOnInit() {
    this.activedRoute.params.subscribe((dataParams)=>{ 
      this.studentService.getStudent().subscribe((data)=>{
      data.map((sinhvien)=>{
        if(sinhvien.idLop == dataParams.id){
          this.mangsinhvien.push(sinhvien)
        }
     
      })
    })
    }
    )
   
  }
  detailStudent(id){
    this.router.navigate([`detailStudent/${id}`]);
  }
}
