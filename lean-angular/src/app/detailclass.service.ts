import { Injectable } from '@angular/core';
import { dataLOP } from './fakeData/lopData';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DetailclassService {

  constructor() { }
  getLop(){
    return of(dataLOP);
  }
}
