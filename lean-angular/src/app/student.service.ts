import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { datadsSinhvien } from './fakeData/dssv';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor() { }
  getStudent(){
    return of(datadsSinhvien);
  }
}
