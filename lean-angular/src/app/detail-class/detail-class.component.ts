import { Component, OnInit } from '@angular/core';
import { DetailclassService } from '../detailclass.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-detail-class',
  templateUrl: './detail-class.component.html',
  styleUrls: ['./detail-class.component.css']
})
export class DetailClassComponent implements OnInit {
  mangLop = [];
  constructor(private lopService:DetailclassService,private router:Router) { }

  ngOnInit() {
    this.lopService.getLop().subscribe((data)=>{
      this.mangLop = data;
    })
  }
  gotoDSSV(id){
    this.router.navigate([`detailDSSV/${id}`]);
  }
}
