import { TestBed } from '@angular/core/testing';

import { DetailclassService } from './detailclass.service';

describe('DetailclassService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DetailclassService = TestBed.get(DetailclassService);
    expect(service).toBeTruthy();
  });
});
