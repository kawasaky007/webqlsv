import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-detail-student',
  templateUrl: './detail-student.component.html',
  styleUrls: ['./detail-student.component.css']
})
export class DetailStudentComponent implements OnInit {
    sv;
  constructor(private actionRouter:ActivatedRoute, private ServiceStudet:StudentService) { }

  ngOnInit() {
    this.actionRouter.params.subscribe((dataParmas)=>{
      this.ServiceStudet.getStudent().subscribe((data)=>{
        data.map((sinhvien)=>{
          if(sinhvien.idStudent==dataParmas.id)
          {
            this.sv=sinhvien;
          }
        })
      })
    })
  }

}
